import re
from datetime import datetime

output = 0
lines = open(input("File to calculate: "), 'r').read().split(",")
solution = lines.pop()
time = datetime.now()
for line in lines:
    if re.match(r"\n?-?[0-9]+\.[0-9]+", line):
        output += float(line)
print(f"Time passed: {datetime.now()-time}")
print("Output: " + str(output).replace('\n', ''))
print("Solution: " + str(solution).replace("\n", ""))
